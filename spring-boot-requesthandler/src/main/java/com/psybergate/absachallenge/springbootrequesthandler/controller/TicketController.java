package com.psybergate.absachallenge.springbootrequesthandler.controller;

import com.psybergate.absachallenge.springbootrequesthandler.model.Ticket;
import com.psybergate.absachallenge.springbootrequesthandler.model.events.Event;
import com.psybergate.absachallenge.springbootrequesthandler.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/ticket")
public class TicketController {

  @Autowired
  private TicketService ticketService;

  @GetMapping("/tickets")
  public List<Ticket> getTickets() {
    return ticketService.retrieveTickets();
  }

  @GetMapping("/assign/{referenceNumber}/{name}")
  public Event assignTicket(@PathVariable("referenceNumber") Long referenceNumber, @PathVariable("name") String name) {
    return ticketService.assignTicket(referenceNumber, name);
  }

  @GetMapping("/finalise/{referenceNumber}")
  public Event finaliseTicket(@PathVariable("referenceNumber") Long referenceNumber) {
    return ticketService.finaliseTicket(referenceNumber);
  }
}
