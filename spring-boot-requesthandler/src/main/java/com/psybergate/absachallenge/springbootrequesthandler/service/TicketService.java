package com.psybergate.absachallenge.springbootrequesthandler.service;

import com.psybergate.absachallenge.springbootrequesthandler.model.Ticket;
import com.psybergate.absachallenge.springbootrequesthandler.model.events.Event;

import java.util.List;

public interface TicketService {

  public void producer(Event event);

  public void consumer(Event event);

  public List<Ticket> retrieveTickets();

  public Event assignTicket(long referenceNumber, String name);

  public Event finaliseTicket(long referenceNumber);

  public void update(Ticket ticket);
}
