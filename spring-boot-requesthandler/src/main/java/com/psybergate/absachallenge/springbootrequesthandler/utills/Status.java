package com.psybergate.absachallenge.springbootrequesthandler.utills;

public class Status {

  public static final String ASSIGNED = "ASSIGNED";

  public static final String COMPLETED = "COMPLETED";
}
