package com.psybergate.absachallenge.springbootrequesthandler.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket {

  @Id
  @Column(name = "referenceNumber")
  private long referenceNumber;

  @Column(name = "status")
  private String status;

  @Column(name = "description")
  private String description;

  @Column(name = "assignedTo")
  private String assignedTo;

  public Ticket() {
  }

  public Ticket(long referenceNumber, String status, String description) {
    this.referenceNumber = referenceNumber;
    this.status = status;
    this.description = description;
  }

  public long getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(long referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAssignedTo() {
    return assignedTo;
  }

  public void setAssignedTo(String assignedTo) {
    this.assignedTo = assignedTo;
  }

  @Override
  public String toString() {
    return "Ticket{" +
        "referenceNumber=" + referenceNumber +
        ", status='" + status + '\'' +
        ", description='" + description + '\'' +
        ", assignedTo='" + assignedTo + '\'' +
        '}';
  }
}
