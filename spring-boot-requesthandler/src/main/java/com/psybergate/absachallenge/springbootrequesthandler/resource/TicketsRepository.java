package com.psybergate.absachallenge.springbootrequesthandler.resource;

import com.psybergate.absachallenge.springbootrequesthandler.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketsRepository extends JpaRepository<Ticket, Long> {
}
