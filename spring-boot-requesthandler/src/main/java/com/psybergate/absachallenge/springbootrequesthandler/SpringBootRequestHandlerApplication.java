package com.psybergate.absachallenge.springbootrequesthandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRequestHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRequestHandlerApplication.class, args);
	}

}
