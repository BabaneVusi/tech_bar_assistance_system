package com.psybergate.absachallenge.springbootrequesthandler.service.impl;

import com.psybergate.absachallenge.springbootrequesthandler.model.Ticket;
import com.psybergate.absachallenge.springbootrequesthandler.model.events.Event;
import com.psybergate.absachallenge.springbootrequesthandler.resource.TicketsRepository;
import com.psybergate.absachallenge.springbootrequesthandler.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static com.psybergate.absachallenge.springbootrequesthandler.utills.Status.ASSIGNED;
import static com.psybergate.absachallenge.springbootrequesthandler.utills.Status.COMPLETED;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

  @Autowired
  private KafkaTemplate<String, Event> kafkaTemplate;

  @Autowired
  private TicketsRepository ticketsRepository;

  private static final String TOPIC = "technician_event";

  /**
   * Retrieves all the tickets stored in the technician database.
   *
   * @return List of tickets
   */
  public List<Ticket> retrieveTickets() {
    return ticketsRepository.findAll();
  }

  /**
   * Subscribes to the specified topic and read incoming data/events.
   * Creates a ticket based on the event details.
   * Calls the save method.
   *
   * @param event
   */
  @KafkaListener(topics = "client_event", groupId = "events", containerFactory =
      "technicianKafkaListenerContainerFactory")
  public void consumer(Event event) {
    Ticket ticket = new Ticket(event.getReferenceNumber(), event.getStatus(), event.getDescription());
    save(ticket);
  }

  /**
   * Saves tickets to the technician database
   *
   * @param ticket
   */
  private void save(Ticket ticket) {
    ticketsRepository.save(ticket);
  }

  /**
   * Assigns a person/technician to a ticket corresponding to the reference number.
   * Creates an event specifying that the event has been assigned.
   * Then calls the producer method.
   *
   * @param referenceNumber
   * @param name
   * @return Event
   */
  public Event assignTicket(long referenceNumber, String name) {
    Ticket ticket = ticketsRepository.findById(referenceNumber).get();
    Event event = createEvent(ticket);
    if (!ticket.getStatus().equals(COMPLETED)) {
      ticket.setStatus(ASSIGNED);
      ticket.setAssignedTo(name);
      event = createEvent(ticket);
      update(ticket);
      producer(event);
    }
    return event;
  }

  /**
   * Updates the ticket status to completed once ticket is finalised.
   * Creates an event specifying that the event has been completed.
   * Then calls the producer method.
   *
   * @param referenceNumber
   * @return Event
   */
  public Event finaliseTicket(long referenceNumber) {
    Ticket ticket = ticketsRepository.findById(referenceNumber).get();
    Event event = createEvent(ticket);
    if (ticket.getStatus().equals(ASSIGNED)) {
      ticket.setStatus(COMPLETED);
      event = createEvent(ticket);
      update(ticket);
      producer(event);
    }
    return event;
  }

  /**
   * Updates the corresponding ticket in the database.
   *
   * @param ticket
   */
  public void update(Ticket ticket) {
    save(ticket);
  }

  /**
   * Creates an event from the ticket details.
   *
   * @param ticket
   * @return Event
   */
  private Event createEvent(Ticket ticket) {
    return new Event(ticket.getReferenceNumber(), ticket.getStatus(), ticket.getDescription());
  }

  /**
   * Writes the event to the Kafka cluster specified topic.
   *
   * @param event
   * @return String
   */
  public void producer(Event event) {
    kafkaTemplate.send(TOPIC, event);
  }
}
