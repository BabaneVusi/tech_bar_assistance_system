# Tech Bar Assistance system

This systems allows a user to log requests and technicians to assign and complete requests.

* The system was developed and ran locally. Kafka and the JDK must be installed locally to run the application. 
* The user and technician systems are written in spring boot.
* The two system are separate applications that communicate via kafka.

# Running the Tech bar system

- Start the zookeeper from the installation folder: kafka-server-start config\server.properties
- Start a bootstrap server from the installation folder: zookeeper-server-start config\zookeeper.properties

- Start the two applications using the main methods.
- The producers and consumers will automatically connect to the bootstrap server.

# spring-boot-requestlogger

* This is the user system that allows users to log requests. 
* It exposes two Rest API's for creating the request and viewing the status of the request. 
* Use Postman or any API testing application that will allow you to interact with the API's. 

- create request: POST method that takes a json body - http://localhost:8081/ticket/create
BODY FORMAT 
{
      "name" : "",
       "surname": "",
       "description": ""
}
- view status: GET request that takes a URL parameter referenceNumber - http://localhost:8081/ticket/status/<referenceNumber> 

# spring-boot-requesthandler

* This is the system that allows techinicians to create, assign and finalise tickets.
* It exposes three Rest API's for retrieving all tickets, assigning and finalising a ticket.
* Use Postman or any API testing application that will allow you to interact with the API.

- view tickets: GET method - http://localhost:8080/ticket/tickets
- assign ticket: GET method that takes two URL parameters referenceNumber and name - http://localhost:8080/ticket/assign/<referenceNumber>/<name>
- finalise ticket: GET method that takes one URL parameter referenceNumber - http://localhost:8080/ticket/finalise/<referenceNumber>