package com.psybergate.absachallenge.springbootrequestlogger.service;

import com.psybergate.absachallenge.springbootrequestlogger.model.Request;
import com.psybergate.absachallenge.springbootrequestlogger.model.events.Event;
import com.psybergate.absachallenge.springbootrequestlogger.model.Status;

public interface RequestService {

  public void producer(Event event);

  public void consumer(Event event);

  public Status retrieveStatus(long referenceNumber);

  public Event create(Request request);
}
