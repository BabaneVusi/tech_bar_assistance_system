package com.psybergate.absachallenge.springbootrequestlogger.service.impl;

import com.psybergate.absachallenge.springbootrequestlogger.model.Request;
import com.psybergate.absachallenge.springbootrequestlogger.model.events.Event;
import com.psybergate.absachallenge.springbootrequestlogger.model.Status;
import com.psybergate.absachallenge.springbootrequestlogger.resource.RequestRepository;
import com.psybergate.absachallenge.springbootrequestlogger.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RequestServiceImpl implements RequestService {

  @Autowired
  private KafkaTemplate<String, Event> kafkaTemplate;

  @Autowired
  private RequestRepository requestRepository;

  private static final String TOPIC = "client_event";

  /**
   * Creates or saves a request in the client database.
   * Calls the create event method.
   * Calls the producer to write the event to the specified topic.
   * @param request
   * @return Event
   */
  @Override
  public Event create(Request request) {
    requestRepository.save(request);
    Event event = createEvent(request);
    producer(event);
    return event;
  }

  /**
   * Creates an event that will be sent via kafka to the target system.
   * @param request
   * @return Event
   */
  public Event createEvent(Request request) {
    Event event = new Event(request.getReferenceNumber(), request.getStatus(), request.getDescription());
    return event;
  }

  /**
   * Writes the event to the Kafka cluster specified topic.
   * @param event
   * @return referenceNumber
   */
  public void producer(Event event) {
    kafkaTemplate.send(TOPIC, event);
  }

  /**
   * Subscribes to the specified topic and read incoming data/events.
   * @param event
   */
  @KafkaListener(topics = "technician_event", groupId = "events_json", containerFactory =
      "requestKafkaListenerContainerFactory")
  public void consumer(Event event) {
    updateRequestStatus(event);
  }

  /**
   * Takes reference number and get the corresponding request from the database.
   * Creates a status with the reference number and the request status.
   * @param referenceNumber
   * @return Status
   */
  @Override
  public Status retrieveStatus(long referenceNumber) {
    Request request = requestRepository.findById(referenceNumber).get();
    Status status = new Status(request.getReferenceNumber(), request.getStatus());
    return status;
  }

  /**
   * Updates the status of the request according to the event details.
   * @param event
   */
  private void updateRequestStatus(Event event) {
    Request request = requestRepository.findById(event.getReferenceNumber()).get();
    request.setStatus(event.getStatus());
    requestRepository.save(request);
  }
}
