package com.psybergate.absachallenge.springbootrequestlogger.model.events;

public class Event {

  private long referenceNumber;

  private String status;

  private String description;

  public Event() {
  }

  public Event(long referenceNumber, String status, String description) {
    this.referenceNumber = referenceNumber;
    this.status = status;
    this.description = description;
  }

  public long getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(long referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "Event{" +
        "referenceNumber=" + referenceNumber +
        ", status='" + status + '\'' +
        ", description='" + description + '\'' +
        '}';
  }
}
