package com.psybergate.absachallenge.springbootrequestlogger.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.psybergate.absachallenge.springbootrequestlogger.utils.Constants.CREATED;

@Entity
@Table(name = "request")
public class Request {

  @Id
  @Column(name = "referenceNumber")
  private long referenceNumber;

  @Column(name = "status", nullable = false)
  private String status;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "surname", nullable = false)
  private String surname;

  @Column(name = "description", nullable = false)
  private String description;

  public Request() {
    setReferenceNumber(hashCode());
    setStatus(CREATED);
  }

  public Request(String name, String surname, String description) {
    this.name = name;
    this.surname = surname;
    this.description = description;
    setReferenceNumber(hashCode());
    setStatus(CREATED);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  private void setReferenceNumber(long referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public long getReferenceNumber() {
    return referenceNumber;
  }

  public String getStatus() {
    return status;
  }
}
