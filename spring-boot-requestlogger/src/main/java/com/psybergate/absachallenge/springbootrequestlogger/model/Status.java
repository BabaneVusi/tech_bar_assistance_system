package com.psybergate.absachallenge.springbootrequestlogger.model;

public class Status {

  private long referenceNumber;

  private String status;

  public Status(long referenceNumber, String status) {
    this.referenceNumber = referenceNumber;
    this.status = status;
  }

  public long getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(long referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
