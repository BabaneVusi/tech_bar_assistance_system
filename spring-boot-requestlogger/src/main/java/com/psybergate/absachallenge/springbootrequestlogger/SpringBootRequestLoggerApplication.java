package com.psybergate.absachallenge.springbootrequestlogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRequestLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRequestLoggerApplication.class, args);
	}
}
