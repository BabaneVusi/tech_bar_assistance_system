package com.psybergate.absachallenge.springbootrequestlogger.controller;

import com.psybergate.absachallenge.springbootrequestlogger.model.Request;
import com.psybergate.absachallenge.springbootrequestlogger.model.events.Event;
import com.psybergate.absachallenge.springbootrequestlogger.model.Status;
import com.psybergate.absachallenge.springbootrequestlogger.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ticket")
public class RequestController {

  @Autowired
  private RequestService requestService;

  @PostMapping("/create")
  public Event create(@RequestBody Request request) {
    requestService.create(request);
    return requestService.create(request);
  }

  @GetMapping("/status/{referenceNumber}")
  public Status getStatus(@PathVariable("referenceNumber") long referenceNumber){
    return requestService.retrieveStatus(referenceNumber);
  }
}
