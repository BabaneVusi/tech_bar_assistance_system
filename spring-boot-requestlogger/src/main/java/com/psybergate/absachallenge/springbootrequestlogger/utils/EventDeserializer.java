package com.psybergate.absachallenge.springbootrequestlogger.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.psybergate.absachallenge.springbootrequestlogger.model.events.Event;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class EventDeserializer implements Deserializer<Event> {

  private ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {

  }

  @Override
  public Event deserialize(String topic, byte[] data) {
    try {
      if (data == null) {
        System.out.println("Null received at deserializing");
        return null;
      }
      System.out.println("Deserializing...");
      return objectMapper.readValue(new String(data, "UTF-8"), Event.class);
    } catch (Exception e) {
      throw new SerializationException("Error when deserializing byte[] to MessageDto");
    }
  }

  @Override
  public void close() {
  }
}
